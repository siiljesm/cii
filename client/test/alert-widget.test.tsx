import * as React from 'react';
import { Alert } from '../src/widgets';
import { shallow } from 'enzyme';

describe('Alert tests', () => {
  test('No alerts initially', () => {
    const wrapper = shallow(<Alert />);

    expect(wrapper.matchesElement(<div></div>)).toEqual(true);
  });

  test('Show alert message', (done) => {
    const wrapper = shallow(<Alert />);

    Alert.danger('test');

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.matchesElement(
          <div>
            <div>
              test
              <button />
            </div>
          </div>
        )
      ).toEqual(true);

      done();
    });
  });

  test('Close alert message', (done) => {
    const wrapper = shallow(<Alert />);

    Alert.danger('test');

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.matchesElement(
          <div>
            <div>
              test
              <button />
            </div>
          </div>
        )
      ).toEqual(true);

      wrapper.find('button.btn-close').simulate('click');

      expect(wrapper.matchesElement(<div></div>)).toEqual(true);

      done();
    });
  });

  test('Display 3 alert messages and close the second', (done) => {
    const wrapper = shallow(<Alert />);

    Alert.danger('testAlert1');
    Alert.danger('testAlert2');
    Alert.danger('testAlert3');

    setTimeout(() => {
      console.log(wrapper.debug());
      expect(
        wrapper.matchesElement(
          <div>
            <div>
              testAlert1
              <button />
            </div>
            <div>
              testAlert2
              <button />
            </div>
            <div>
              testAlert3
              <button />
            </div>
          </div>
        )
      ).toEqual(true);

      wrapper.find('button.close').at(1).simulate('click');

      expect(
        wrapper.matchesElement(
          <div>
            <div>
              testAlert1
              <button />
            </div>
            <div>
              testAlert3
              <button />
            </div>
          </div>
        )
      ).toEqual(true);
      done();
    });
  });
});

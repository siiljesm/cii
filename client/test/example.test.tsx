import * as React from 'react';
import { Component } from 'react-simplified';
import { Column, Button, Form, Card, Row } from '../src/widgets';
import { TaskList } from '../src/task-components';
import { shallow } from 'enzyme';
import taskService from '../src/task-service';

class Edit extends Component {
  text = '';

  render() {
    return (
      <Form.Input
        type="text"
        value={this.text}
        onChange={(event) => (this.text = event.currentTarget.value)}
      />
    );
  }
}

class Hello extends Component {
  render() {
    return (
      <div>
        <b>Hello</b>
      </div>
    );
  }
}

describe('Column Widget test', () => {
  test('Column draws correctly', () => {
    const wrapper = shallow(<Column>test</Column>);
    expect(
      wrapper.matchesElement(
        <div className="col">
          <div>test</div>
        </div>
      )
    ).toEqual(true);
  });

  test('Column draws correctly when width is set', () => {
    const wrapper = shallow(<Column width={2}>test</Column>);
    expect(
      wrapper.matchesElement(
        <div className="col-2">
          <div>test</div>
        </div>
      )
    ).toEqual(true);
  });

  test('Column draws correctly when right is set', () => {
    const wrapper = shallow(<Column right>test</Column>);
    expect(
      wrapper.matchesElement(
        <div>
          <div className="float-end">test</div>
        </div>
      )
    ).toEqual(true);
  });
});

describe('Hello test', () => {
  test('Hello test', () => {
    const wrapper = shallow(<Hello />);
    expect(
      wrapper.matchesElement(
        <div>
          <b>Hello</b>
        </div>
      )
    ).toEqual(true);
  });
});

//simulering av museklikk
describe('test mouseclick', () => {
  test('tests mouseclick', () => {
    let buttonClicked = false;
    const wrapper = shallow(
      <Button.Success onClick={() => (buttonClicked = true)}>test</Button.Success>
    );

    expect(buttonClicked).toEqual(false);
    wrapper.find('button').simulate('click');
    expect(buttonClicked).toEqual(true);
  });
});

//simulering av input-hendelser
describe('test input', () => {
  test('test input', () => {
    const wrapper = shallow(<Edit />);

    // @ts-ignore
    expect(wrapper.instance().text == '').toEqual(true);

    wrapper.find(Form.Input).simulate('change', { currentTarget: { value: 'test' } });

    // @ts-ignore
    expect(wrapper.instance().text == 'test').toEqual(true);
  });
});

//teste kolonner annen måte
describe('Column Widget tests 2', () => {
  test('Draws correctly', () => {
    const wrapper = shallow(<Column>test</Column>);
    //console.log(wrapper.debug());
    expect(wrapper).toMatchSnapshot();
  });

  test('Draws correctly when width is set', () => {
    const wrapper = shallow(<Column width={2}>test</Column>);
    expect(wrapper).toMatchSnapshot();
  });

  test('Draws correctly when right is set', () => {
    const wrapper = shallow(<Column right>test</Column>);
    expect(wrapper).toMatchSnapshot();
  });
});

// describe('test components', () => {
//   test('test taskList', (done) => {
//     const wrapper = shallow(<TaskList />);

//     setTimeout(() => {
//       console.log(wrapper.debug());
//       expect(
//         wrapper.containsMatchingElement(
//           <Card>
//             <Row>
//               <Column>
//                 <NavLink>Les leksjon</NavLink>
//               </Column>
//             </Row>
//             <Row>
//               <Column>
//                 <NavLink>Gjør øving</NavLink>
//               </Column>
//             </Row>
//           </Card>
//         )
//       ).toEqual(true);
//       done();
//     }, 1000);
//   });
// });

//mocke service-klassen
// jest.mock('../src/task-sevice', () => {
//   class TaskService {
//     getAll() {
//       return Promise.resolve([
//         { id: 1, title: 'Les leksjon', done: false },
//         { id: 2, title: 'Gjør øving', done: false },
//       ]);
//     }
//   }
//   return new TaskService();
// });

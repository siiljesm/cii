import * as React from 'react';
import { Component } from 'react-simplified';
import { Column, Button, Form, Card, Row } from '../src/widgets';
import { TaskDetails } from '../src/task-components';
import { shallow } from 'enzyme';
import taskService from '../src/task-service';

test('Check if TaskDetails title draws correctly', () => {
  const wrapper = shallow(<TaskDetails match={{ params: { id: 1 } }} />);

  setTimeout(() => {
    expect(
      wrapper.containsMatchingElement([
        <Row>
          <Column width={2}>Title:</Column>
          <Column>Les leksjon</Column>
        </Row>,
      ])
    ).toEqual(true);
  });
});

test('Check if TaskDetailt title draws correctly', () => {
  const wrapper = shallow(<TaskDetails match={{ params: { id: 1 } }} />);
  expect(wrapper).toMatchSnapshot();
});
